/*
 * Copyright (c) 2010-2012 The Amdatu Foundation
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.kitchensink.demo.tenant.local.impl;

import java.util.Dictionary;

import org.amdatu.kitchensink.demo.tenant.local.LocalApplication;
import org.apache.felix.dm.Component;
import org.osgi.service.cm.ConfigurationException;
import org.osgi.service.cm.ManagedService;

/**
 * @author <a href="mailto:amdatu-developers@amdatu.org">Amdatu Project Team</a>
 */
public class LocalApplicationImpl implements ManagedService, LocalApplication {

    public static final String CONFIG_PID = "org.amdatu.kitchensink.demo.tenant.local";
    public static final String CONFIG_MSG_KEY = "demo.tenant.msg";
    public static final String MSG_DEFAULT = "Default message";

    private volatile String m_helloWorldMessage = MSG_DEFAULT;
    private volatile Component m_component;

    public void init() {
        setMessageProperty(m_helloWorldMessage);
    }

    public String helloWorld() {
        return m_helloWorldMessage;
    }

    public void updated(Dictionary properties) throws ConfigurationException {
        String msg = MSG_DEFAULT;
        if (properties != null) {
            Object value = properties.get(CONFIG_MSG_KEY);
            if (value == null) {
                throw new ConfigurationException(CONFIG_MSG_KEY,
                    "Configuration value is missing");
            }
            if (!(value instanceof String)) {
                throw new ConfigurationException(CONFIG_MSG_KEY,
                    "Configuration value must be String");
            }
            msg = (String) value;
        }
        m_helloWorldMessage = msg;
        setMessageProperty(m_helloWorldMessage);
    }

    private void setMessageProperty(String msg) {
        Dictionary props = m_component.getServiceProperties();
        props.put(CONFIG_MSG_KEY, m_helloWorldMessage);
        m_component.setServiceProperties(props);
    }
}
