#!/bin/sh

CURRENT_DIR=`pwd`
if [ "${AMDATU_HOME}" == "" ]; then
  echo AMDATU_HOME not set, using current working directory...
  AMDATU_HOME=$CURRENT_DIR
fi

WORK_DIR=$AMDATU_HOME/work
PLATFORM_DIR=$AMDATU_HOME/servers
DEMO_DIR=$AMDATU_HOME/demos
DEPLOY_DIR=$AMDATU_HOME/deploy

platformName=minimal
# Parse commandline arguments...
while getopts hclp: ARG; do
	case "$ARG"
	in
		c) if [ -d "${WORK_DIR}" ]; then
				echo "Cleaning working directory: ${WORK_DIR}."
				rm -rf ${WORK_DIR};
			fi;;
		l)  echo "Available platforms: $(ls -m $PLATFORM_DIR)";
			exit 0;;
		p)  platformName=$OPTARG;
			if [ ! -d "${PLATFORM_DIR}/$platformName" ]; then
				echo "Platform not found: $platformName.";
				exit 1;
			fi
			;;
		h|[?])  echo "$0 [options]\n -h\tshow this help message;\n -c\tclean caches before starting platform;\n -p\tselect a platform (default is minimum);\n -l\tlist all available platforms.\n";
			exit 1
			;;
	esac
done
# Remove all parameters
shift $OPTIND-1

# Open a debug port
JAVA_OPTS="-Xdebug -Xrunjdwp:transport=dt_socket,address=8000,server=y,suspend=n"

# Set memory options
JAVA_OPTS="$JAVA_OPTS -Xms256m -Xmx1024m -XX:MaxPermSize=256m"

# Felix property file
JAVA_OPTS="$JAVA_OPTS -Dfelix.config.properties=file:conf/amdatu-platform.properties -Dfelix.fileinstall.dir=$PLATFORM_DIR/$platformName,$DEPLOY_DIR" 

# Set encoding to UTF-8
JAVA_OPTS="$JAVA_OPTS -Dfile.encoding=utf-8"

# Run the platform...
echo "Starting Amdatu server ($platformName)..."
java $JAVA_OPTS -jar lib/org.apache.felix.main-4.0.2.jar

###EOF###